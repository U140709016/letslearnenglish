package com.example.btamer.letslearnenglish.numbers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.btamer.letslearnenglish.R;
import com.example.btamer.letslearnenglish.family.Family;
import com.example.btamer.letslearnenglish.family.Level1family;

public class Numbers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);
        Button b=(Button) findViewById(R.id.play_number);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Numbers.this ,number2.class);
                startActivity(intent);
            }
        });

    }
}
