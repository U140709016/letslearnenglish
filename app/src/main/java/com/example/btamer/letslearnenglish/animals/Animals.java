package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.btamer.letslearnenglish.MainActivity;
import com.example.btamer.letslearnenglish.R;

public class Animals extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animals);
        Button plybtn = (Button) findViewById(R.id.play_animal);
        plybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Animals.this, Cat.class);
                startActivity(intent);
            }
        });

    }
}
