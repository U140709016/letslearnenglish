package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Donkey extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donkey);
        final MediaPlayer donkeysound;

        donkeysound=MediaPlayer.create(Donkey.this , R.raw.donkey);
        ImageButton btn1=(ImageButton)findViewById(R.id.donkeyimagebutton);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                donkeysound.start();

            }
        });

        Button cow= (Button)findViewById(R.id.buttoncow3);
        cow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button butterfly= (Button)findViewById(R.id.buttonbutterfly3);
        butterfly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button snake = (Button)findViewById(R.id.buttonsnake );
        snake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button donkey1= (Button)findViewById(R.id.btndnkey1);
        donkey1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                donkeysound.stop();
                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Donkey.this,Bird.class);
                startActivity(intent);
            }

        });
    }


}

