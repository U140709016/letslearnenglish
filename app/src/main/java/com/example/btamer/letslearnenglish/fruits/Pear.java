package com.example.btamer.letslearnenglish.fruits;


  import android.content.Intent;
  import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
  import android.view.KeyEvent;
  import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

  import com.example.btamer.letslearnenglish.MainActivity;
  import com.example.btamer.letslearnenglish.R;
  import com.example.btamer.letslearnenglish.family.Family;

public class  Pear extends AppCompatActivity implements View.OnClickListener{

        private TextView wordTv;
        private EditText wordEnteredTv;
        private Button check, Change_word,back_menu;
        private String wordToFind;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_pear);
            wordTv = (TextView) findViewById(R.id.wordTv);
            wordEnteredTv = (EditText) findViewById(R.id.wordEnteredEt);
            check= (Button) findViewById(R.id.validate);
            check.setOnClickListener(this);
            wordEnteredTv.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // If the event is a key-down event on the "enter" button
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        validate();
                        Toast.makeText(Pear.this, wordEnteredTv.getText(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                    return false;
                }
            });
           back_menu = (Button) findViewById(R.id.back_menu1);
            back_menu.setOnClickListener(this);
            Change_word();
        }

        @Override
        public void onClick(View view) {
            if (view == check) {
                validate();

            }else if (view == back_menu){
                Intent intent = new Intent(Pear.this,MainActivity.class);
                startActivity(intent);
                    }

            }


        private void validate() {
            String w = wordEnteredTv.getText().toString();

            if (wordToFind.equals(w)) {
                Toast.makeText(this, "Congratulations ! You found the word " + wordToFind, Toast.LENGTH_SHORT).show();
                Change_word();
            } else {
                Toast.makeText(this, "False !", Toast.LENGTH_SHORT).show();
            }
        }

        private void Change_word() {

           wordToFind = WordGame.randomWord();
            String wordShuffled = WordGame.shuffleWord(wordToFind);
            wordTv.setText(wordShuffled);
            wordEnteredTv.setText("");
        }
    }