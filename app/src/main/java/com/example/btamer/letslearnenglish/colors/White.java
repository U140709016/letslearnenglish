package com.example.btamer.letslearnenglish.colors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.Congrats;
import com.example.btamer.letslearnenglish.R;

public class White extends AppCompatActivity implements View.OnClickListener {
    Button btntrue, btn1,btn2,btn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white);

        btn1 = (Button)findViewById(R.id.white1);
        btn1.setOnClickListener(this);

          btn2= (Button)findViewById(R.id.white4);
        btn2.setOnClickListener(this);
        btn3 = (Button)findViewById(R.id.white3);
        btn3.setOnClickListener(this);
       btntrue= (Button)findViewById(R.id.whitetrue);
        btntrue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()== btntrue.getId()){
             Toast.makeText(getApplicationContext(), "True", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(White.this, Congrats.class);

                startActivity(intent);}
            else {  Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();}


    }
}