package com.example.btamer.letslearnenglish.family;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.btamer.letslearnenglish.Congrats;
import com.example.btamer.letslearnenglish.MainActivity;
import com.example.btamer.letslearnenglish.R;

public class Family extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        Button b=(Button) findViewById(R.id.play_family);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Family.this ,Level1family.class);
                startActivity(intent);
            }
        });
    }
}
