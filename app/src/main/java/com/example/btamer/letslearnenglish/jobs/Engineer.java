package com.example.btamer.letslearnenglish.jobs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Engineer extends AppCompatActivity implements View.OnClickListener{
    EditText ed;
    Button btn;
    String job;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engineer);
        ed= (EditText) findViewById(R.id.edc);
        btn=(Button) findViewById(R.id.btnc);
        job = "engineer";
        ed.setOnClickListener(this);
        ed .setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    onClick (ed);
                    Toast.makeText(Engineer.this, ed.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });


        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String w = ed.getText().toString();
        if (w.equals(job)) {
            Intent intent = new Intent(Engineer.this,Doctor.class);
            startActivity(intent);

        }}

}
