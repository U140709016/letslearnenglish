package com.example.btamer.letslearnenglish.jobs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.btamer.letslearnenglish.Congrats;
import com.example.btamer.letslearnenglish.R;

public class SoccerPlayer extends AppCompatActivity implements View.OnClickListener{
    EditText ed;
    Button btn;
    String job, job2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soccer_player);
        ed= (EditText) findViewById(R.id.edj);
        btn=(Button) findViewById(R.id.btnj);
        job = "soccer player";
        job2= "football player";
        ed.setOnClickListener(this);
        ed .setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    onClick (ed);

                    return true;
                }
                return false;
            }
        });

        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String w = ed.getText().toString();
        if (w.equals(job)) {
            Intent intent = new Intent(SoccerPlayer.this, Congrats.class);
            startActivity(intent);}
        else if (w.equals(job2)) {
            Intent intent = new Intent(SoccerPlayer.this, Congrats.class);
            startActivity(intent);

        }}

}
