package com.example.btamer.letslearnenglish.colors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Green extends AppCompatActivity implements View.OnClickListener{
    Button btntrue, btn1,btn2,btn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_green);

        btn1 = (Button)findViewById(R.id.green1);
        btn1.setOnClickListener(this);

         btn2= (Button)findViewById(R.id.green2);
        btn2.setOnClickListener(this);  btn3 = (Button)findViewById(R.id.green3);
         btntrue= (Button)findViewById(R.id.greentrue);
        btntrue.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if (v.getId()== btntrue.getId()){
            Toast.makeText(getApplicationContext(), "True", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Green.this, Black.class);
            startActivity(intent);}
        else {  Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();}


    }
}