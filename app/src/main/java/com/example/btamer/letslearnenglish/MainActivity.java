package com.example.btamer.letslearnenglish;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.animals.Animals;
import com.example.btamer.letslearnenglish.colors.Color;
import com.example.btamer.letslearnenglish.family.Family;
import com.example.btamer.letslearnenglish.fruits.Fruits;
import com.example.btamer.letslearnenglish.jobs.Jobs;
import com.example.btamer.letslearnenglish.numbers.Numbers;
import com.example.btamer.letslearnenglish.objects.Objects;

import java.util.List;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button animals = (Button) findViewById(R.id.btnanimal);
        animals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,Animals.class);
                startActivity(intent);
            }
        });
        Button family = (Button) findViewById(R.id.fmlybtn);

        family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,Family.class);
                startActivity(intent);
            }
        });

        final Button number = (Button) findViewById(R.id.btnnumber);

        number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,Numbers.class);
                startActivity(intent);
            }
        });
        Button fruits= (Button) findViewById(R.id.fruitbtn);
       fruits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,Fruits.class);
                startActivity(intent);
            }
        });
        Button Jobs= (Button) findViewById(R.id.jobbtn);

        Jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, Jobs.class);
                startActivity(intent);
            }
        });
        final Button Colors = (Button) findViewById(R.id.colorbtn);

        Colors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent( MainActivity.this , Color.class);
                startActivity(intent);
            }
        });
        Button Objects = (Button) findViewById(R.id.objectbtn);
     //   objectsound= MediaPlayer.create(MainActivity.this , R.raw.);
        Objects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, Objects.class);
                startActivity(intent);
            }
        });


    }


}
