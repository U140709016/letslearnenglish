package com.example.btamer.letslearnenglish.jobs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.btamer.letslearnenglish.MainActivity;
import com.example.btamer.letslearnenglish.R;
import com.example.btamer.letslearnenglish.fruits.Fruits;

public class Jobs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);
        Button lets_play= (Button) findViewById(R.id.play_jobs);
       lets_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Jobs.this,Fireman.class);
                startActivity(intent);
            }
        });
    }
}
