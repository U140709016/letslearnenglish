package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Bird extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bird);
        final MediaPlayer birdsound;

        birdsound=MediaPlayer.create(Bird.this , R.raw.soundbird);
        ImageButton btn=(ImageButton)findViewById(R.id.birdimgbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                birdsound.start();
                Toast.makeText(getApplicationContext(), "CHIRP CHIRP",Toast.LENGTH_SHORT).show();
            }
        });


        Button butterfly = (Button)findViewById(R.id.btnbutterfly3);
        butterfly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button cat= (Button)findViewById(R.id.btncat2);
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button dog = (Button)findViewById(R.id.buttdog2 );
        dog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button bird= (Button)findViewById(R.id.buttonbird3);
       bird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Bird.this, Horse.class);
                birdsound.stop();
                startActivity(intent);
            }

        });
    }
}


