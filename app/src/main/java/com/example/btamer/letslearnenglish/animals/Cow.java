package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Cow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow);
        final MediaPlayer sound;

        sound=MediaPlayer.create(Cow.this , R.raw.ccow);
        ImageButton btn=(ImageButton)findViewById(R.id.cowimgbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.start();
                Toast.makeText(getApplicationContext(), "MEUH ",Toast.LENGTH_SHORT).show();
            }
        });


        Button horse = (Button)findViewById(R.id.cowhorse);
        horse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button bird= (Button)findViewById(R.id.cowbird);
        bird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button cat = (Button)findViewById(R.id.cowcat);
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button cow= (Button)findViewById(R.id.cowcow);
        cow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.stop();
                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Cow.this,Bee.class);
                startActivity(intent);
            }

        });
    }
}


