package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Bee extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bee);



        ImageButton btn=(ImageButton)findViewById(R.id.beeimgbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "BZZZ",Toast.LENGTH_SHORT).show();
            }
        });


        Button bird = (Button)findViewById(R.id.beebird);
        bird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button dog= (Button)findViewById(R.id.beedog);
        dog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button butterfly = (Button)findViewById(R.id.beebutterfly );
        butterfly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button bee= (Button)findViewById(R.id.beebee);
        bee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Bee.this, dog.class);
                startActivity(intent);

            }

        });
    }
}


