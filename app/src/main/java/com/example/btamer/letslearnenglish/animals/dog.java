package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.Congrats;
import com.example.btamer.letslearnenglish.R;

public class dog extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog);
        final MediaPlayer sound;

        sound=MediaPlayer.create(dog.this , R.raw.dogsound);
        ImageButton btn=(ImageButton)findViewById(R.id.dogimgbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.start();
                Toast.makeText(getApplicationContext(), "Haw ",Toast.LENGTH_SHORT).show();
            }
        });


        Button button1 = (Button)findViewById(R.id.dogcow);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button button2= (Button)findViewById(R.id.dogbird);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button button3  = (Button)findViewById(R.id.dogcat);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button button4= (Button)findViewById(R.id.dogdog);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.stop();
                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(dog.this,Congrats.class);
                startActivity(intent);
            }

        });
    }
}


