package com.example.btamer.letslearnenglish.fruits;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.btamer.letslearnenglish.R;
import com.example.btamer.letslearnenglish.animals.Animals;
import com.example.btamer.letslearnenglish.animals.Cat;

public class Fruits extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruits);
        Button plybtn = (Button) findViewById(R.id.play_fruits);
        plybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Fruits.this, Pear.class);
                startActivity(intent);
            }
        });
    }
}
