package com.example.btamer.letslearnenglish.animals;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Butterfly extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butterfly);
        final MediaPlayer butterflypro;

            butterflypro=MediaPlayer.create(Butterfly.this , R.raw.butterfly);
            ImageButton btn=(ImageButton)findViewById(R.id.buterflyimagebutton);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    butterflypro.start();

                }
            });
            Button bird = (Button)findViewById(R.id.buttonbird2);
            bird.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
                }
            });
            Button bee= (Button)findViewById(R.id.buttonbee2);
            bee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
                }
            });
            Button cat= (Button)findViewById(R.id.buttoncat2 );
            cat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
                }
            });
            Button butterfly= (Button)findViewById(R.id.buttonbutterfly2);
            butterfly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    butterflypro.stop();
                    Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Butterfly.this,Donkey.class);
                    startActivity(intent);
                }

            });
        }
    }

