package com.example.btamer.letslearnenglish.family;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.Congrats;
import com.example.btamer.letslearnenglish.MainActivity;
import com.example.btamer.letslearnenglish.R;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;

public class levels extends AppCompatActivity {
    ImageView iv11,iv12,iv21,iv22,iv31,iv32;
    Integer[] cardsArray = {101 ,102,103,201,202,203};
    int image101, image102, image103, image201,image202,image203;

    int firstcard,secondcard;
    int clickedFirst, clilckedSecond;
    int cardNumber=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);
        iv11 = (ImageView) findViewById(R.id.iv11);
        iv12 = (ImageView) findViewById(R.id.iv12);
        iv22 = (ImageView) findViewById(R.id.iv22);
        iv21 = (ImageView) findViewById(R.id.iv21);
        iv31 = (ImageView) findViewById(R.id.iv31);
        iv32 = (ImageView) findViewById(R.id.iv32);

        iv11.setTag("0");
        iv12.setTag("1");
        iv21.setTag("2");
        iv22.setTag("3") ;
        iv31.setTag("4");
        iv32.setTag("5");
        frontOfCardsResources();
        Collections.shuffle(Arrays.asList(cardsArray));
        iv11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(iv11, theCard);
            }
        });
        iv12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(iv12, theCard);
            }
        });
        iv21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(iv21, theCard);
            }
        });
        iv22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(iv22, theCard);
            }
        });
        iv31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(iv31, theCard);
            }
        });
        iv32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(iv32, theCard);
            }


        });
    }

    private void doStuff(ImageView iv,int card) {


        //set the correct image
        if (iv==iv11) {
            iv.setImageResource(image101);
            cardsArray[0] = 101;
        } else if (iv==iv12) {
            iv.setImageResource(image102);
            cardsArray[1] = 102;
        } else if (iv==iv21) {
            iv.setImageResource(image103);
            cardsArray[2] = 103;
        } else if( iv==iv22) {
            iv.setImageResource(image201);
            cardsArray[3] = 201;
        } else if (iv==iv31) {
            iv.setImageResource(image202);
            cardsArray[4] = 202;
        } else if (iv==iv32) {
            iv.setImageResource(image203);
            cardsArray[5] = 203;
        }

        //check which image is selected
        if (cardNumber == 1) {
            firstcard = cardsArray[card];
              /*if (firstcard > 200) {
                   firstcard = firstcard - 100;
               }*/
            cardNumber ++ ;
            clickedFirst = card ;
            iv.setEnabled(false);


        } else if (cardNumber == 2) {
            secondcard = cardsArray[card];
               /*if (secondcard < 200) {
                   secondcard = secondcard - 100;
               }*/
            cardNumber ++;
            clilckedSecond = card ;



            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    calculate();

                }
            }, 1000);
        }
        else{



            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {


                }
            }, 1000);


            cardNumber =1;
        }
    }

    private void calculate (){
        if (firstcard==101 && secondcard==201){
            if (clickedFirst== 0) {
                iv11.setVisibility(View.INVISIBLE);
            }
            if (clilckedSecond== 3) {
                iv22.setVisibility(View.INVISIBLE);
            }
        }
        else {
            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);}



        if (firstcard==103 && secondcard==203){
            if (clickedFirst== 2){
                iv21.setVisibility(View.INVISIBLE);
            }
            if (clilckedSecond== 5) {
                iv32.setVisibility(View.INVISIBLE);

            }}else {
            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);}




        if (firstcard==102 && secondcard==202){
            if (clickedFirst==1){
                iv12.setVisibility(View.INVISIBLE);
            } if (clilckedSecond== 4) {
                iv31.setVisibility(View.INVISIBLE);

            }}else {
            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);}



        if (firstcard==201 && secondcard==101){
            if (clickedFirst== 3){
                iv22.setVisibility(View.INVISIBLE);
            }if (clilckedSecond== 0) {
                iv11.setVisibility(View.INVISIBLE);

            }}else {
            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);}




        if (firstcard==203 && secondcard==103){
            if (clickedFirst== 5){
                iv32.setVisibility(View.INVISIBLE);
            }if (clilckedSecond==2) {
                iv21.setVisibility(View.INVISIBLE);

            }}else {
            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);}





        if (firstcard==202 && secondcard==102){
            if (clickedFirst== 4){
                iv31.setVisibility(View.INVISIBLE);
            } if (clilckedSecond== 1) {
                iv12.setVisibility(View.INVISIBLE);

            }
        }else {
            iv11.setImageResource(R.color.greenw);
            iv12.setImageResource(R.color.mypurple);
            iv21.setImageResource(R.color.mypink);
            iv22.setImageResource(R.color.myturkuaz);
            iv31.setImageResource(R.color.myyellow);
            iv32.setImageResource(R.color.colorPrimaryDark);
        }

        iv11.setEnabled(true);
        iv12.setEnabled(true);
        iv21.setEnabled(true);
        iv22.setEnabled(true);
        iv31.setEnabled(true);
        iv32.setEnabled(true);

        checkEnd();

    }

    private void checkEnd() {
        if (iv11.getVisibility() == View.INVISIBLE &&
                iv12.getVisibility() == View.INVISIBLE &&
                iv21.getVisibility() == View.INVISIBLE &&
                iv22.getVisibility() == View.INVISIBLE &&
                iv31.getVisibility() == View.INVISIBLE &&
                iv32.getVisibility() == View.INVISIBLE) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(levels.this);
            alertDialogBuilder
                    .setPositiveButton("Game over ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(levels.this, Congrats.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });

            AlertDialog alertDialog =alertDialogBuilder.create();
            alertDialog.show();

        }}


    private void frontOfCardsResources() {
        image101 = R.drawable.banne;
        image102 = R.drawable.bbaba;
        image103 = R.drawable.uncle;
        image201 = R.drawable.gmother;
        image202 = R.drawable.gfather;
        image203 = R.drawable.amca;

    }


}
