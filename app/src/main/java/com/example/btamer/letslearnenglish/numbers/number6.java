package com.example.btamer.letslearnenglish.numbers;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class number6 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number6);
        final MediaPlayer sound;

        sound=MediaPlayer.create(number6.this , R.raw.six);
        ImageButton btn=(ImageButton)findViewById(R.id.img6);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.start();

            }
        });


        Button btn1 = (Button)findViewById(R.id.a6);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button btn2= (Button)findViewById(R.id.d6);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button btn3 = (Button)findViewById(R.id.c6);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button btn4= (Button)findViewById(R.id.bt6);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.stop();
                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(number6.this,number1.class);
                startActivity(intent);
            }

        });
    }
}


