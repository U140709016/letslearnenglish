package com.example.btamer.letslearnenglish.animals;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.btamer.letslearnenglish.R;

public class Cat extends AppCompatActivity {
    MediaPlayer catsound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat);

        catsound =MediaPlayer.create(Cat.this , R.raw.orcatsound);
        ImageButton btn=(ImageButton)findViewById(R.id.catimagebutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catsound.start();
                Toast.makeText(getApplicationContext(), "Meoww",Toast.LENGTH_SHORT).show();

            }
        });
        Button bird= (Button)findViewById(R.id.buttonbird);
        bird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button dog= (Button)findViewById(R.id.buttondog);
        dog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button donkey= (Button)findViewById(R.id.buttondonkey );
        donkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "FALSE!",Toast.LENGTH_SHORT).show();
            }
        });
        Button cat= (Button)findViewById(R.id.buttoncat);
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catsound.stop();
                Toast.makeText(getApplicationContext(), "True",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Cat.this,Butterfly.class);
                startActivity(intent);
            }

        });
    }


}
